﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STO
{
    class Customer
    {
        public string Name { get; private set; }
        public string Address { get; private set; }
        public string Phone { get; private set; }
       
        public Customer(string name, string address, string phone)
        {
            Name = name;
            Address = address;
            Phone = phone;
        }
    }
}
