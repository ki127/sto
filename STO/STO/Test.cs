﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STO
{
    class Test
    {
        public Sto generate()
        {
            Sto sto = new Sto();
            AddServices(sto.GetServiceList());
            AddOrders(sto, sto.GetServiceList());
            return sto;
        }

        private void AddServices(ServiceList sl)
        {
            Service s1 = new Service(400,"замена лобового стекла",".........",".......\\image.jpg");
            Service s2 = new Service(800,"замена переднего бампера",".........",".......\\image2.jpg");
            Service s3 = new Service(1000,"замена тормозов",".........",".......\\image3.jpg");

            sl.AddService(s1);
            sl.AddService(s2);
            sl.AddService(s3);
        }

        private void AddOrders(Sto sto, ServiceList sl)
        {
            Customer c1 = new Customer("Vanya", "dom 3", "333-33-33");
            Customer c2 = new Customer("Petya", "dom 5", "333-33-34");

            Account acc1 = new Account(c1);
            Account acc2 = new Account(c2);

            acc1.AddOrder(sl.GetService("замена лобового стекла"), sto.GetOrdersCount());
            foreach (Order order in acc1.Orders)
                sto.AddOrder(order);
            acc2.AddOrder(sl.GetService("замена переднего бампера"), sto.GetOrdersCount());
            acc2.AddServiceToOrder(sl.GetService("замена тормозов"));
            foreach (Order order in acc2.Orders)
                sto.AddOrder(order);

            sto.AddAccount(acc1);
            sto.AddAccount(acc2);

                
            
        }
    }
}
