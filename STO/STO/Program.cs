﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STO
{
    class Program
    {
        private Sto sto = new Sto();
        static void Main(string[] args)
        {
            Program p = new Program();
            p.FillTestModel();
            p.GenerateModelReport();
        }

        private void FillTestModel()
        {
            Test generator = new Test();
            sto = generator.generate();
        }

        private void GenerateModelReport()
        {
            Report generator = new Report(Console.Out, sto);
            generator.generate();
        }
    }
}
