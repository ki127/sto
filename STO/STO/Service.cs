﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STO
{
    class Service
    {
        public int price {get; private set;}

        public string name { get; private set; }

        public string description { get; private set; }

        public string imageURL { get; private set; }

        public bool hidden { get; set; }

        public Service(int _price, string _name, string _description, string _imageURL)
        {
            price = _price;
            name = _name;
            description = _description;
            imageURL = _imageURL;
            hidden = false;
        }

    }
}
