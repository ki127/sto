﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STO
{
    class Account
    {
        public Customer customer { get; private set; }

        private List<Order> orders;

        public Account(Customer _customer)
        {
            customer = _customer;
            orders = new List<Order>();
        }

        public IEnumerable<Order> Orders
        {
            get { return orders; }
        }

        public void AddOrder(Service _service, int id)
        {
            Order order = new Order(id);
            orders.Add(order);
            AddServiceToOrder(_service);
        }

        public void AddServiceToOrder(Service _service)
        {
            if (!orders.Last().isReady)
                orders.Last().AddService(_service);
        }
    }
}
