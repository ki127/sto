﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STO
{
    class Sto
    {
        private ServiceList serviceList;

        private List<Order> orders;

        private List<Account> accounts;

        public Sto()
        {
            serviceList = new ServiceList();
            orders = new List<Order>();
            accounts = new List<Account>();
        }

        public IEnumerable<Account> Accounts
        {
            get { return accounts; }
        }

        public void AddAccount(Account account)
        {
            accounts.Add(account);
        }

        public IEnumerable<Order> Orders
        {
            get { return orders; }
        }

        public ServiceList GetServiceList()
        {
            return serviceList;
        }

        public int GetOrdersCount()
        {
            return orders.Count;
        }

        public void AddOrder(Order order)
        {
            orders.Add(order);
        }
    }
}
