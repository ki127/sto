﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STO
{
    class ServiceList
    {
        private List<Service> services;

        public ServiceList()
        {
            services = new List<Service>();
        }

        public IEnumerable<Service> Services
        {
            get { return services; }
        }

        public int GetServicesCount()
        {
            return services.Count;
        }

        public void AddService(Service _service)
        {
            services.Add(_service);
        }

        public void DeleteService(Service _service)
        {
            services.Remove(_service);
        }

        public Service GetService(string _service)
        {
            return services.Find((service) => service.name == _service);
        }
    }
}
